.286    ;DECLARAMOS LA ARQUITECTURA DE LA MAQUINA
;FUNCION DE LA FORMA (A+B-D)
;AUTOR: TLALI SANTIAGO
pila SEGMENT STACK ;DECLARAMOS EL SEGMENTO DE PILA (CUANTOS ESPACIO EN MEMORIA SE NECESITA PARA EL PROGRAMA)
	DB 32 DUP ('stack___')
pila ends     ; FIN DEL SEGMENTO DE PILA

Datos SEGMENT   ;INICIO DEL SEGMENTOS DE DATOS
    
	letrero1 DB 'Primer Termino:','$' ; LETREROS 
    letrero2 DB 'Segundo Termino:', '$'  
	letrero3 DB 'Tercer Termino:','$'
	letrero4 DB 'Suma:','$'
	letrero5 DB 'Resultado:','$'
	terminoA DB ('?'), '$'    ;ESPACIOS DE MEMORIA
	terminoB DB ('?'), '$'
	terminoC DB ('?'), '$'
	sum DB ('?'),'$'
	salto DB 0DH, 0AH, '$'  ;SALTO DE LINEA
	
Datos ends ; FIN DEL SEGEMENTO DE DATOS


codigo SEGMENT 'Code'  ;INICIA EL SEGMENTO DE CODIGO
	Assume ss:pila, ds:datos, cs:codigo  
	
 Main PROC FAR 
	
	PUSH DS ;GUARDA DIRECCCION DE RETORNO AL SISTEMA OPERATIVO
	PUSH 0
	MOV AX, datos
	MOV DS, AX
;------------------------------------------------------------	
	MOV AH,09H  ; FUNCION 09H DE LA INT 21H GUARDA EN DS:DX
	LEA DX, letrero1    
	INT 21H			;VISUALIZAR UNA CADENA 
	MOV AH, 01H  ;FUNCION 01H ENTRADA DE CARACTER 
	INT 21H  ;LO RESERVA EN AL
	
	SUB AL, 30H       ;RESTAMOS 30H PARA OBTENER EL NUMERO
	MOV terminoA, AL  ; GUARDAMOS EN termino1 LO QUE HAY EN AL
	
	MOV AH, 09H    ;SALTO DE LINEA
	LEA DX, salto
	INT 21H
	MOV AH, 09H
	LEA DX, letrero2 ;''
	INT 21H
	MOV AH, 01H
	INT 21H      ;''
	
	SUB AL, 30H
	MOV terminoB, AL 
	
	MOV BL, terminoA     ; SE ASIGNA A BL LO QUE HAY EN AL
	
;-----------------------INICIO DE SUMA------------------------------------------	
	ADD BL, terminoB ;(ADD Destino, Origen) : Resultado en el Operando DESTINO
	MOV sum, BL
	
	MOV AH, 09H
	LEA DX, salto
	INT 21H
	MOV AH, 09H
	LEA DX, letrero4
	INT 21H
	
	MOV DL, sum
	ADD DL, 30H
	
	
	MOV AH, 02H ;IMPIRMIR CARACTER
	INT 21H 
    
	 
    MOV AH, 09H
	LEA DX, salto
	INT 21H	 
;--------------------------FIN DE SUMA-------------------------------	 
	
	MOV AH, 09H
	LEA DX, letrero3 ; PIDO EL TERCER TERMINO
	INT 21H
	
	MOV AH, 01H ;ENTRADA DE CARACTER RESREVO EN AL
	INT 21H
	SUB AL, 30H     ;OBTENGO EL NUMERO

	MOV terminoC, AL
;------------------------INICIO DE LA RESTA ---------------------------	
	MOV BL, sum      ;MUEVO SUM AL REGISTRO BL
	SUB BL, terminoC ; RESTO EL terminoC  ALA SUMA EN BL
	
	MOV AH, 09H
	LEA DX, salto 
	INT 21H

	
	MOV AH, 09H
	LEA DX, letrero5
	INT 21H
	
	MOV DL, BL    ;MUEVO BL A DL PARA PODER IMPRIMIR EL CARACTER
	ADD DL, 30H
	MOV AH, 02H
	INT 21H
	
	MOV AH, 09H
	LEA DX, salto 
	INT 21H
	
	MOV AH, 4CH
	INT 21H 
;-----------------------------FIN DE LA RESTA -----------------
Main Endp
codigo ends
end Main
