.286

pila segment stack
db 32 dup('stack_')
pila ends


data segment
    mensaje1 db 'INGRESA EL NUMERO: $', 0dh, 0ah
    feliz db 'feliz$',0dh, 0ah
    infeliz db 'infeliz$',0dh, 0ah
    cadena db 3 dup(0)
    n1 db 0
    n2 db 0
    n3 db 0
    suma1 db 0
    suma2 db 0
    suma3 db 0
    auxiliar db 0
    contador db 0
data ends


code segment para 'code'
inicio proc far

assume cs:code,ds:data,ss:pila

push ds
push 0
mov ax,data
mov ds,ax

mov contador,0
iniciar:
;limpiar pantalla
mov ah, 00
mov al, 03h
int 10h

mov di,0                   
mov cl,0

mov ah,09h                                      ;se imprime la cadena de lo que se requiere
mov dx, offset mensaje1
int 21h

lectura:                    ;se leen los valores por teclado (uno por uno)
mov ah,01h
int 21h     

cmp al,0dh                  ;si es un enter finaliza la lectura
je empieza                  ;	Salto si igual (equal), si cero (zero), si ZF=1.


mov cadena[di],al           ;se van ingresando al arreglo 'cadena' los valores uno por uno
inc di                      ;incremento de 'di' para ingresar el proximo caracter en la siguiente posicion

inc cl
cmp cl,2                    ;si el contador de digitos es igual a 2 empieza los calculos
je empieza

jmp lectura


empieza:
cmp cl, 2
jb iniciar ;Salto si menor (below), si no superior ni igual (not above or equal), si acarreo, si CF=1.	

mov di,0
mov al, cadena[di]
sub al,30h
cmp al,4
ja iniciar    ;Salto si mayor (above), si no menor o igual (not below or equal), si CF=0 y ZF=0.
je control
jb validar

jmp operaciones

control:
mov bl,1

validar:
inc di
mov al,cadena[di]
sub al,30h
cmp bl,1
jne operaciones ;Salta si no es igual o salta si no es cero.
cmp al, 4
ja inicio

operaciones:

mov al, cadena[0]
sub al,30h
mov n1,al

mov al, cadena[1]
sub al,30h
mov n2,al


mov cl, n1
sumar1:
mov al,n1
add al,suma1
mov suma1,al
loop sumar1


mov cl, n2
sumar2:
mov al,n2
add al,suma2
mov suma2,al
loop sumar2

add al, suma1
mov auxiliar,al

aam
mov n1, al     ;de un número, por ejemplo 42, te separa en AL y AH los dígitos… 
mov n2,ah     ;quedando en AH=4 y en AL= 2. Todo esto para poder reasignar valores a n1 y n2
			; retomando el ejemplo de 42, n1= 4 y n2=2, y volver a meterlo en un ciclo para poder sacar una nueva sumatoria de cuadrados.


mov ah,02h
mov dl,0ah ;salto de l?nea
int 21h 

mov ah,02h
mov dl,n2
add dl,30h
int 21h

mov ah,02h
mov dl,n1
add dl,30h
int 21h

mov suma1,0
mov suma2,0

jmp ciclo
;-----------------------

ciclode3:

aam
mov n1,al
mov al,ah
aam
mov n2,al
mov n3,ah

mov ah,02h
mov dl,0ah ;salto de l?nea
int 21h 

mov ah,02h
mov dl,n3
add dl,30h
int 21h

mov ah,02h
mov dl,n2
add dl,30h
int 21h

mov ah,02h
mov dl,n1
add dl,30h
int 21h

mov suma1,0
mov suma2,0
mov suma3,0

ciclo2:
inc contador
mov cl, n1
sumar5:
mov al,n1
add al,suma1
mov suma1,al
loop sumar5

mov cl, n2
sumar6:
mov al,n2
add al,suma2
mov suma2,al
loop sumar6

mov cl, n3
sumar7:
mov al,n3
add al,suma3
mov suma3,al
loop sumar7

add al,suma2
add al,suma1

cmp al,99
ja ciclodetres
cmp al,1
je fin
cmp al,auxiliar
je salir21
cmp contador,8
je salir21
jmp sigue

fin:
jmp salir

ciclodetres:
jmp ciclode3
;--------------------------
ciclo:
inc contador
mov cl, n1
sumar3:
mov al,n1
add al,suma1
mov suma1,al
loop sumar3

mov cl, n2
sumar4:
mov al,n2
add al,suma2
mov suma2,al
loop sumar4

add al, suma1
cmp al,99
ja ciclodetres
cmp al,1  ; Cuando era igual a 1
je salir
cmp al,auxiliar
je salir2
cmp contador,8   ;cuando se calculaba 8 veces la sumatoria y no se definía si era feliz todavía
je salir2

sigue:
aam
mov n1,al
mov n2,ah

mov ah,02h
mov dl,0ah ;salto de l?nea
int 21h 

mov ah,02h
mov dl,n2
add dl,30h
int 21h
mov ah,02h
mov dl,n1
add dl,30h
int 21h
mov suma1,0
mov suma2,0
jmp ciclo

salir21:
jmp salir2

salir:
mov ah,02h
mov dl,0ah ;salto de l?nea
int 21h 
mov ah,09h                                      ;se imprime la cadena 
mov dx, offset feliz
int 21h
mov ax,4c00h            ;fin de programa
int 21h
salir2:
mov ah,02h
mov dl,0ah ;salto de l?nea
int 21h 
mov ah,09h                                      ;se imprime la cadena
mov dx, offset infeliz
int 21h
mov ax,4c00h            ;fin de programa
int 21h


inicio endp
code ends

end inicio