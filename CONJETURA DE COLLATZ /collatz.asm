.286 
;CONJETURA DE COLLATZ  
;AUTOR : TLALI SANTIAGO
pila SEGMENT STACK
	DB 32 DUP ('stack___')
pila ENDS

Datos SEGMENT
	letrero1 db 'Ingrese Numero: ','$'
	letrero2 db 13,10,'Division: ','$'
	letrero3 DB 13,10,'Multiplicacion:','$'
	letrero4 DB 13,10,'Repeticion ^','$'
	letPar DB 10,13,' es Par','$'
	letImp DB  10,13,' es Impar','$'
	multiplicador db 3
	divisor db 2 
	var1 db 0
Datos ENDS

Codigo SEGMENT 'Code'  ;INICIA EL SEGMENTO DE CODIGO
	Assume ss:pila, ds:datos, cs:codigo  
	
 Main PROC FAR 
	
	PUSH DS ;GUARDA DIRECCCION DE RETORNO AL SISTEMA OPERATIVO
	PUSH 0
	MOV AX, datos
	MOV DS, AX
;------------------------------------------------------------	
	MOV AH,09h
	LEA DX, letrero1 ;DESPLEGAR LETRERO1
	INT 21h
	MOV AH, 01H ;ENTRADA DE CARACTER
	INT 21H   

inicio:
	SUB AL,30h 	;RESTA 30H PARA OBTENER CARACTER
	MOV var1,AL ;SE GUARDA AL EN var1
	MOV BL,divisor ;2 SE MUEVE A BL PARA SER OPERADO
	XOR AX,AX 	;lSE LIMPIA EL REGISTRO AX 
	MOV AL,divisor
	MOV BL,AL
	MOV AL,var1
	div BL 		; DIVIDE AL/BL 
	CMP AH, 0	; COMPARACION DEL RESIDUO DX
	JZ par      ; SALTO A LA ETIQUETA SI ES PAR
    JMP impar	; SALTO A LA ETIQUETA SI ES IMPAR
	
par: 
    MOV AH, 09H    
	LEA DX, letPar   	;IMPRIME PAR
    INT 21h	
	MOV AH,09h
	LEA DX,letrero2 	;DESPLEGAR LETRERO DE DIVISION
	INT 21h
	
	MOV BL,AL  
	MOV DL, BL
	ADD DL,30h          ;SUMA 30H PARA OBTENER CARACTER
	MOV AH,02h			; SE IMPRIME LA DIVISION
    INT 21h
	 
	MOV var1,DL         ;PASAMO A DL A VAR1 
    MOV AL, var1		;MOVEMOS var1 A AL PARA PODER IR LA ETIQUETA INICIO:
	 
	JMP inicio			; SALTO A LA ETIQUETA INICIO
	;jmp salir 
impar:
        MOV AH,09H
		LEA DX, letImp
        INT 21h
        
		MOV AH,09h
	    LEA DX,letrero3 	;MENSAJE DE MULTIPLICACION
	    INT 21h
		
	    MOV AL,var1
		MOV BL, multiplicador	;PASAMOS EL MULTPLICADO 3 A BL   
		MUL BL					;MULTIPLICA AL=AL*BL
		ADD AL, 1				;SE SUMA AL +1 (N*3)+1
		MOV DL,AL
		
		ADD DL, 30h				;SUMA 30H PARA OBTENER CARACTER
	
		MOV AH, 02h				; SE IMPRIME LA MULTIPLICACION
		INT 21h
		
		MOV AH,09h
		LEA DX,letrero4 ;MENSAJE DE REPETICIÓN
		INT 21h	
		jmp inicio	
		JMP salir     ; SI REPITE TERMINA LA EJECUCION
salir:    	
	MOV AH, 4CH
	INT 21H 
;-----------------------------FIN DE LA EJECUCIÓN -----------------
Main Endp
Codigo ends
end Main
